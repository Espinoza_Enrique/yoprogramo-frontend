/*Recupero todas las secciones de la página*/
let secciones=document.getElementsByTagName("section");
let elementosNav=document.getElementsByClassName("navbar-nav");
let botonesMod=document.getElementsByClassName("btn-mod")
let opcionSeleccionada=0;
document.onload(prepararDocumento());

/*Emplea el número de sección asignado a cada uno según la disposición dentro del DOM. */
function marcarSeleccionado(id){
    let elementosA=elementosNav.item(0).getElementsByTagName("a");
    let claseReemplazada="";
    /*
    for (let i=0;i<elementosA.length;i++){
        //alert("Entra en el buccle 1")
        if(i==numeroSeccion && numeroSeccion!=-1){
           
            claseReemplazada=elementosA.item(i).getAttribute("class");
            elementosA.item(i).setAttribute("class",claseReemplazada+" active")
        } else{
            
            claseReemplazada=elementosA.item(i).getAttribute("class");
            
            claseReemplazada=reemplazarCadena("active","",claseReemplazada);
           
            elementosA.item(i).setAttribute("class",claseReemplazada);
            
        }
    }*/
    for (let i=0;i<elementosA.length;i++){
            claseReemplazada=elementosA.item(i).getAttribute("class");
            claseReemplazada=reemplazarCadena("active","",claseReemplazada);
            elementosA.item(i).setAttribute("class",claseReemplazada)
        }
    document.getElementById(id).setAttribute("class",document.getElementById(id).getAttribute("class")+" active");
   
 }

function mostrarSeccion(numeroSeccion){
    //alert(secciones.length);
    let id;
    for(let i=0;i<secciones.length;i++){
        let clase=secciones.item(i).getAttribute("class");
       
        let cadenaReemplazada=reemplazarCadena("shinobi","",clase);
        
        if(i==numeroSeccion){
            secciones.item(i).setAttribute("class",cadenaReemplazada);
            id=secciones.item(i).getAttribute("id");
            
        }else{
            secciones.item(i).setAttribute("class",cadenaReemplazada+" shinobi");
        }
    }
    return id.slice(id.indexOf("-")+1,id.length);
}




function mostrarVista(numeroSeccion){
     if (numeroSeccion!==0){
        colapsarNavegador();
     }
    
    marcarSeleccionado(mostrarSeccion(numeroSeccion));
    
    
}

function loguear(){
    let usuario=document.getElementById("usuarioLogin");
    let contra=document.getElementById("contraLogin");
    if(usuario.value=="Admin" && contra.value=="1234567"){
        if (window.sessionStorage) {
              sessionStorage.setItem("login", "true");
              location.href="./index.html";
            
        }
    else

     {

      throw new Error('Tu Browser no soporta sessionStorage!');

    }
       
    }else{
        alert("La contraseña o usuario son incorrectos");
    }

}

function mostrarLoginCierre()
{
   
    let claseLogin=document.getElementById("liIniciarSesion").getAttribute("class");
    let claseCerrar=document.getElementById("liCerrarSesion").getAttribute("class");
    let nuevaClaseLogin
    let nuevaClaseCerrar

    if (!getEstadoLogueo()){
        
        nuevaClaseCerrar=reemplazarCadena("eureka","shinobi",claseCerrar);
        nuevaClaseLogin=reemplazarCadena("shonobi","eureka",claseLogin);
        
    }else{
        nuevaClaseCerrar=reemplazarCadena("shinobi","eureka",claseCerrar);
        nuevaClaseLogin=reemplazarCadena("eureka","shinobi",claseLogin);
        
    }
    document.getElementById("liCerrarSesion").setAttribute("class",nuevaClaseCerrar);
    document.getElementById("liIniciarSesion").setAttribute("class",nuevaClaseLogin);
}
function cerrarSesion()
{
    //result = confirm("¿Desea cerrar sesión?");
    //if (result){
        sessionStorage.removeItem("login");
        location.href="./index.html";
    //}
    
       
}
function reemplazarCadena(cadenaVieja, cadenaNueva, cadenaCompleta) {
    // Reemplaza cadenaVieja por cadenaNueva en cadenaCompleta
    
       for (var i = 0; i < cadenaCompleta.length; i++) {
          if (cadenaCompleta.substring(i, i + cadenaVieja.length) == cadenaVieja) {
             cadenaCompleta= cadenaCompleta.substring(0, i) + cadenaNueva + cadenaCompleta.substring(i + cadenaVieja.length, cadenaCompleta.length);
          }
       }
       return cadenaCompleta;
    }


function getEstadoLogueo(){
   
       let logueo=sessionStorage.getItem("login");
       if(logueo!==null && logueo==="true"){
           return true;
       }else{
           return false
       }
  
}

function activarBotonesMod(){
    let claseAplicada;
    let claseQuitada;
    let nuevaClaseAplicada;
    let claseVieja;
   
    if(getEstadoLogueo()){
        claseAplicada="btn-mod eureka d-flex justify-content-center";
        claseQuitada="btn-mod shinobi"
    }else{
        claseAplicada="btn-mod shinobi";
        claseQuitada="btn-mod eureka d-flex justify-content-center";
    }

    for (let i=0;i<botonesMod.length;i++){
       
        claseVieja=botonesMod.item(i).getAttribute("class");
       
        nuevaClaseAplicada=reemplazarCadena(claseQuitada,claseAplicada,claseVieja);
        botonesMod.item(i).setAttribute("class",nuevaClaseAplicada);
        
    }
}

function establecerBanner(){
    if (!getEstadoLogueo()){
        rutaBanner="./assets/img/argpro-icono-1.png";
    }else{
        rutaBanner="./assets/img/argpro-icono-2.png";
    }
    document.getElementById("img-banner").setAttribute("src",rutaBanner);
    
}

function prepararDocumento(){

    mostrarLoginCierre();
    activarBotonesMod();
    establecerBanner();
   
    
    
  
}

function mostrarCambio(){
    
    let lblExp=document.getElementById("lblPorcentajeExp");
    let rangoExp=document.getElementById("rngExp");
    lblExp.innerHTML=rangoExp.value+" % Experiencia"
}
function colapsarNavegador(){
    
    let dimensiones=window.innerWidth;
    /*Revisa las dimensiones de la ventana actual. Si es menor que un cierto valor, después de 
    haber elegido la opción de navegación la barra colapsa porque el usuario obligatoriamente tuvo
    que hacer clic en el botón para desplegar las opciones.
    Con esto se evitan efectos indeseados en las pantallas mayores.*/
    if(dimensiones<999){
        $('#colapseNavPrincipal').collapse('toggle');
    }
    

}